# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import logging
import requests
from .. import get_statsd_client
from requests.exceptions import ConnectTimeout, HTTPError


class HandlerApi:
    """Handles HTTP POST to specified url and holds configuration for handler."""

    HTTP_STATUS_OK = 200
    __slots__ = [
        "_request_class",
        "_timeout",
        "_certificate",
        "_logger",
        "_statsd",
    ]

    def __init__(
        self, request_timeout: int, request_module=None, certificate=None
    ):
        """Set configuration for handler.

        :param request_timeout: Time in seconds before POST request timeout
        :type request_timeout: int
        :param request_module: module to make requests with defaults to requests
        :param request_module: module
        :param certificate: path to certificate.crt defaults to False
        :param certificate: str
        """
        self._logger = logging.getLogger()
        self._statsd = get_statsd_client()
        self._timeout = request_timeout

        self._request_class = (
            requests if request_module is None else request_module
        )

        self._certificate = False if certificate is None else certificate

    def execute(self, message, routing_key: str):
        """HTTP POST call to the url specified in the message.

        Based on succesfull completion or Exception raised, queueconsumer will
        ack or nack the message.

        :param message: json decoded message, required to include an 'url'
        :type message: dict
        :param routing_key: RabbitMQ routing key associated with message
        :type routing_key: str

        :raises Exception: If `self._request_class.post` call raises an exception.
        :raises Exception: If json() fails on the result from request.post func
        :raises HTTPError: HTTP status != HTTP_STATUS_OK - request did not
            execute as expected
        """
        try:
            url = message["url"]
        except KeyError as e:
            self._logger.warning("No dispatch URL found in message.")
            raise e

        del message["url"]

        self._logger.info(
            f"Dispatching message to '{url}", extra={"queue_url": url}
        )

        try:
            r = self._request_class.post(
                url=url,
                json=message,
                verify=self._certificate,
                timeout=self._timeout,
            )
        except ConnectTimeout as e:
            self._logger.warning(
                f"HTTP timeout after exceeding the time limit: {self._timeout} seconds."
            )
            raise e
        except Exception as e:
            self._logger.warning(f"Error during http(s) request: {e}.")
            raise e

        try:
            self._handle_response(response=r, routing_key=routing_key)
        except Exception as e:
            raise e

    def _handle_response(self, response, routing_key):
        """Handle, log and parse response from HTTP.post call.

        :param response: response from HTTP call
        :type response: response object
        :param routing_key: routing key identifier, used for logging
        :type routing_key: str

        :raises HTTPError: if request unsuccessful error is raised so message
            can be nacked by consumer
        """
        request_id = (
            response.headers["zs-req-id"]
            if "zs-req-id" in response.headers
            else "unknown"
        )
        if response.status_code == self.HTTP_STATUS_OK:
            self._successful_response(
                response=response,
                request_id=request_id,
                routing_key=routing_key,
            )
        else:
            self._unsuccessful_response(
                response=response, request_id=request_id
            )
            raise HTTPError("request did not execute as expected")

    def _successful_response(self, response, request_id, routing_key):
        """Handle response originating from a successful HTTP call.

        :param response: response from HTTP call
        :type response: response object
        :param request_id: request id from HTTP.headers
        :type request_id: str
        :param routing_key: routing key identifier, used for logging
        :type routing_key: str
        """
        self._logger.info(
            "Queue item handled successfully.",
            extra={"queue_url": response.url, "request_id": request_id},
        )
        try:
            # setting json response can result in Value- or KeyError
            response.json()["result"][0].get("status", "unknown")
        except (ValueError, KeyError):
            self._logger.warning(
                "Corrupt response body: '{}'".format(response.text),
                extra={"queue_url": response.url, "request_id": request_id},
            )

    def _unsuccessful_response(self, response, request_id):
        """Handle response originating from an unsuccessful HTTP call.

        :param response: response from HTTP call
        :type response: response object
        :param request_id: request id from HTTP.headers
        :type request_id: str
        """
        base = f"Error while running action ({response.status_code}: {response.reason})"
        try:
            # setting json response can result in Value- or KeyError
            json_response = response.json()["result"][0]

            log_msg = "{}: exception '{}': {}".format(
                base,
                json_response["type"],
                ", ".join(json_response["messages"]),
            )
        except (ValueError, KeyError):
            log_msg = f'{base}: "{response.text}"'

        self._logger.warning(
            log_msg,
            extra={"queue_url": response.url, "request_id": request_id},
        )
