# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import os
from .api_request import HandlerApi


def handler_config():
    """Return config on which handler to bind to queue, routingkey & exchange.

    :return: list of config dicts
    :rtype: list
    """
    return [
        {
            "queue": "zaaksysteem-queue-runner",
            "exchange": os.environ.get("MQ_EXCHANGE", "amq.topic"),
            "exchange_type": "topic",
            "routing_key": "zs.v0.#",
            "handler_class": HandlerApi(
                request_timeout=600,
                certificate=os.environ.get("REQUESTS_CA_BUNDLE"),
            ),
        }
    ]
