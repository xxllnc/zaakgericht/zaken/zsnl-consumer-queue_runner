# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

### Stage 1: Setup base python with requirements
FROM python:3.9-slim-bullseye AS base

# Add necessary packages to system
RUN apt-get update && apt-get install -y \
  git \
  netcat \
  psmisc

COPY requirements/base.txt /tmp/requirements.txt

RUN python -m venv /opt/virtualenv \
  && . /opt/virtualenv/bin/activate \
  && pip install -r /tmp/requirements.txt

ENV PATH="/opt/virtualenv/bin:$PATH"

COPY . /opt/queue_runner_v2
WORKDIR /opt/queue_runner_v2

## Stage 2: Production build
FROM python:3.9-slim-bullseye AS production

ENV OTAP=production
ENV PYTHONUNBUFFERED=on
ENV PATH="/opt/virtualenv/bin:$PATH"

RUN apt-get update && apt-get install -y netcat

# Set up application
COPY --from=base /opt /opt/

WORKDIR /opt/queue_runner_v2
RUN pip install -e .
ENTRYPOINT ["/opt/queue_runner_v2/startup.sh"]

## Stage 3: QA environment
FROM base AS quality-and-testing

ENV OTAP=test

COPY requirements/test.txt /tmp/requirements-test.txt
RUN pip install -r /tmp/requirements-test.txt \
 && pip install -e .

RUN  mkdir -p /root/test_result \
  && bin/git-hooks/pre-commit -c -j /root/test_result/junit.xml

ENTRYPOINT ["/opt/queue_runner_v2/startup.sh"]

## Stage 4: Development image
FROM quality-and-testing AS development

ENV OTAP=development

COPY requirements/dev.txt /tmp/requirements-dev.txt
RUN pip install -r /tmp/requirements-dev.txt
