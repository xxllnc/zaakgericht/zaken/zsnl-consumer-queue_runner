# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import unittest.mock as mock


def test_main_function_call():
    from queue_runner_v2.__main__ import main
    from queue_runner_v2.queue_consumer import QueueConsumer

    with mock.patch(
        "queue_runner_v2.queue_consumer.queue_consumer_factory"
    ) as mocked_queue_consumer_factory:

        consumer = mock.MagicMock(spec=QueueConsumer, return_value=True)
        mocked_queue_consumer_factory.return_value = consumer
        main(["--rabbitmq-url", "rabbut_mqtesturl", "--debug"])
        assert mocked_queue_consumer_factory.called


def test_main_init():
    import queue_runner_v2.__main__ as queue_runner

    with mock.patch.object(queue_runner, "main", return_value=42):
        with mock.patch.object(queue_runner, "__name__", "__main__"):
            with mock.patch.object(queue_runner.sys, "exit") as mock_exit:
                queue_runner.init()

                assert mock_exit.call_args[0][0] == 42
